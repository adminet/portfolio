import React from "react";
import styles from "./about.scss";

const About = () => (
  <div className={styles.about} id="about">
    <h2>About me</h2>
    <p>
      For two years I've been creating websites and learning about new web
      development technologies. I created online stores based on wordpress with
      my own design and several business card pages. In my projects, I focus on
      the principle of mobile first, optimization, code semantics and
      availability. I am currently working in London as a freelancer, but I am
      interested in working for a company with team of people.
    </p>
  </div>
);

export default About;
