import * as React from "react";
import makeCarousel from "react-reveal/makeCarousel";
import Slide from "react-reveal/Slide";
import styles from "./slides.scss";
import arrow from "../img/arrow.svg";
import dotOn from "../img/regtOn.svg";
import dotOff from "../img/regtOff2.svg";
import Fosca from "./Fosca/fosca.js";
import Game from "./Game/game.js";
import Animation from "./Koduje/koduje.js";
import Mja from "./Mja/mja.js";

const CarouselUI = ({ total, position, handleClick, children }) => (
  <div className={styles.container}>
    {children}
    <div className={styles.navDots}>
      <div
        className={styles.nextArrow}
        onClick={handleClick}
        data-position={position - 1}
      >
        <img className={styles.arrow} alt="Previous slide" src={arrow} />
      </div>
      <div className={styles.dots}>
        {Array(...Array(total)).map((val, index) => (
          <div
            className={styles.dot}
            key={index}
            onClick={handleClick}
            data-position={index}
          >
            {index === position ? (
              <img className={styles.dot} alt="Actual slide" src={dotOn} />
            ) : (
              <img className={styles.dot} alt="Next slide" src={dotOff} />
            )}
          </div>
        ))}
      </div>
      <div
        className={styles.prevArrow}
        onClick={handleClick}
        data-position={position + 1}
      >
        <img className={styles.arrow} alt="Next slide" src={arrow} />
      </div>
    </div>
  </div>
);
const Carousel = makeCarousel(CarouselUI);

const CarouselContainer = () => (
  <Carousel defaultWait={10000000}>
    <Slide right big>
      <Fosca />
    </Slide>
    <Slide right big>
      <Mja />
    </Slide>
    <Slide right big>
      <Game />
    </Slide>
    <Slide right big>
      <Animation />
    </Slide>
  </Carousel>
);

export default CarouselContainer;
