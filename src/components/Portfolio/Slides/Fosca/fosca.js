import React, { Component } from "react";
import styles from "../../portfolio.scss";
import ViewFosca from "./fosca_modal.js";

class Fosca extends Component {
  state = {
    isOpen: false
  };

  render() {
    return (
      <div className={styles.examplesWorks + " " + styles.bgFosca}>
        <ViewFosca
          isOpen={this.state.isOpen}
          isClose={e => this.setState({ isOpen: false })}
        />
        <div className={styles.exampleDescription}>
          <h3> Websites on Wordpress </h3>
          <p>
            The graphic design and implementation was all my own work. Basic
            knowledge of PHP has helped me write features that change template
            behavior. The store is waiting for additional materials from the
            owners and is not yet launched.
          </p>
          <button
            className={styles.linkToExample}
            onClick={e => this.setState({ isOpen: true })}
          >
            Show me
          </button>
        </div>
      </div>
    );
  }
}

export default Fosca;
