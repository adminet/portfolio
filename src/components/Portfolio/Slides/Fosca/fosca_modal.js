import React, { Component } from "react";
import styles from "../../portfolio.scss";

class ViewFosca extends Component {
  render() {
    let modalFosca = (
      <div className={styles.foscaGif}>
        <button
          className={styles.closeButtonModal}
          onClick={this.props.isClose}
        >
          x
        </button>
      </div>
    );

    if (!this.props.isOpen) {
      modalFosca = null;
    }

    return <div>{modalFosca}</div>;
  }
}

export default ViewFosca;
