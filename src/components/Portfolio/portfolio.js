import * as React from "react";
import styles from "./portfolio.scss";
import CarouselContainer from "./Slides/slides.js";

const Portfolio = () => (
  <section className={styles.sectionPortfolio} id="portfolio">
    <div className={styles.container}>
      <h2> Portfolio </h2>
      <CarouselContainer />
    </div>{" "}
  </section>
);

export default Portfolio;
