import * as React from "react";
import Sections from "../Sections/sections.js";
import { scaleRotate as Menu } from "react-burger-menu";
import styles from "./burgermenu.scss";
import burgerIcon from "./img/burgerIcon.svg";
import crossIcon from "./img/crossIcon.svg";

class BurgerMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false
    };
  }

  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen });
  }

  closeMenu = () => {
    this.setState({ menuOpen: false });
  };

  toggleMenu() {
    this.setState(state => ({ menuOpen: !state.menuOpen }));
  }

  render() {
    return (
      <div id="outer-container" className={styles.outerContainer}>
        <Menu
          right
          burgerButtonClassName={styles.bmBurgerButton}
          crossButtonClassName={styles.bmCrossButton}
          menuClassName={styles.bmMenu}
          overlayClassName={styles.bmOverlay}
          className={styles.menuContainer}
          pageWrapId={"page-wrap"}
          outerContainerId={"outer-container"}
          customBurgerIcon={
            <object type="image/svg+xml" data={burgerIcon} aria-label="Menu" />
          }
          customCrossIcon={
            <object type="image/svg+xml" data={crossIcon} aria-label="Close" />
          }
          isOpen={this.state.menuOpen}
          onStateChange={state => this.handleStateChange(state)}
        >
          <ul className={styles.ul}>
            <li>
              <a
                href="#home"
                className=" menu-item "
                onClick={() => this.closeMenu()}
              >
                Home
              </a>
            </li>
            <li>
              <a
                href="#about"
                className=" menu-item "
                onClick={() => this.closeMenu()}
              >
                About me
              </a>
            </li>
            <li>
              <a
                href="#skills"
                className=" menu-item "
                onClick={() => this.closeMenu()}
              >
                Skills
              </a>
            </li>
            <li>
              <a
                href="#portfolio"
                className=" menu-item "
                onClick={() => this.closeMenu()}
              >
                Portfolio
              </a>
            </li>
            <li>
              <a
                href="#contact"
                className=" menu-item "
                onClick={() => this.closeMenu()}
              >
                Contact
              </a>
            </li>
          </ul>
        </Menu>
        <main id="page-wrap" className={styles.pageWrap}>
          <Sections />
        </main>
      </div>
    );
  }
}

export default BurgerMenu;
