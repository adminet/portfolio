import * as React from "react";
import MetaTags from "react-meta-tags";

class Meta extends React.Component {
  render() {
    return (
      <MetaTags>
        <title>Adrian Kasina | Web Developer | Portfolio Website</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta
          name="description"
          content="WordPress & Front-end Developer producing high quality, responsive and accessible websites."
        />
        <meta
          name="title"
          content="Adrian Kasina | Portfolio, Front-end Developer"
        />
        <link rel="shortcut icon" href="/favicon.ico" />
      </MetaTags>
    );
  }
}

export default Meta;
