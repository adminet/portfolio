import * as React from "react";
import styles from "./skills.scss";
import Icons from "./Icons/icons";

const Skills = () => (
  <section className={styles.sectionSkills} id="skills">
    <div className={styles.container}>
      <h2>Skills</h2>
      <Icons />
      <div className={styles.skillText}>
        <p>
          In most cases, I design the look of a page myself. For this I used
          Photoshop (graphics modifications), Affinity Desinger (raster graphics
          and vector graphics), for mockup: Adobe XD, marvelapp.com,
          www.figma.com. To improve my work I used task and build runners: Gulp,
          Webpack. I created websites based on Bootstrap. The adventure with
          React is just starting, beginning on this page.
        </p>
      </div>
    </div>
  </section>
);

export default Skills;
