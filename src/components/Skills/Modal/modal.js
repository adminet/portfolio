import * as React from "react";
import styles from "./modal.scss";

const ReactMarkdown = require("react-markdown");

function Modal(props) {
  return (
    <article className={styles.modalContainer}>
      <ReactMarkdown source={props.textSouroce} />
    </article>
  );
}

export default Modal;
