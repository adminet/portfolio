import * as React from "react";
import styles from "./header.scss";
import arrowDown from "../../assets/imgs/arrow.svg";
import gitLabIcon from "./img/gitlab.svg";
import headerPict from "./img/header_tablet.svg";

const Header = () => (
  <header className={styles.header} id="home">
    <div className={styles.titleContainer}>
      <span>Hey,</span>
      <h1 className={styles.title}>
        <span>I'm</span>
        Adrian Kasina
      </h1>
      <span>Web Developer</span>
      <p>
        I am happy to work with a client or employer. I created this site in
        React to refine my knowledge and try this library. I also want to show
        my level of knowledge with a web developer using this page. Below is a
        link to the source code.
      </p>
      <a
        href="https://gitlab.com/adminet/portfolio"
        target="_blank"
        rel="noopener noreferrer"
        className={styles.iconGitLab}
      >
        <img src={gitLabIcon} alt="GitLab repository" />
        GitLab
      </a>
    </div>
    <img
      src={headerPict}
      alt="Drawing diagrams and tablet"
      className={styles.headerPict}
    />
    <nav className={styles.buttonsNav} />
    <img className={styles.arrowDown} src={arrowDown} alt="" />
  </header>
);

export default Header;
