import React, { Component } from "react";
import styles from "./menu.scss";

class MenuList extends Component {
  render() {
    return (
      <ul className={styles.ul}>
        <li>
          <a href="#about" onClick={this.isClose}>
            About me
          </a>
        </li>
        <li>
          <a href="#skills" className=" menu-item ">
            Skills
          </a>
        </li>
        <li>
          <a href="#portfolio" className=" menu-item ">
            Portfolio
          </a>
        </li>
        <li>
          <a href="#contact" className=" menu-item ">
            Contact
          </a>
        </li>
      </ul>
    );
  }
}

export default MenuList;
