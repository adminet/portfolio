import * as React from "react";
import MetaHead from "../metatags";
import styles from "./app.scss";
import BurgerMenu from "../Burgermenu/burgermenu.js";

const App = () => (
  <div className={styles.mainContainer}>
    <MetaHead />
    <BurgerMenu />
  </div>
);

export default App;
