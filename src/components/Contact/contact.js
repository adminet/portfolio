import * as React from "react";
import styles from "./contact.scss";
import { withFormik, Field } from "formik";
import * as Yup from "yup";
import classnames from "classnames/bind";

const cx = classnames.bind(styles);

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    name: Yup.string().required("Name is required."),
    subject: Yup.string().required("Subject required."),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required!"),
    message: Yup.string()
      .min(10, "Message is too short")
      .required("Message is required!")
  }),

  mapPropsToValues: ({ user }) => ({
    ...user
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(payload.message);
    setSubmitting(false);
  },
  displayName: "contactForm"
});

const InputFeedback = ({ error }) =>
  error ? <div className={styles.inputFeedback}>{error}</div> : null;

const Label = ({ error, className, children, ...props }) => {
  return (
    <label className="label" {...props}>
      {children}
    </label>
  );
};

const TextInput = ({
  type,
  id,
  name,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => {
  const classes = cx(
    "input-group",
    {
      "animated shake error": !!error
    },
    className
  );
  return (
    <div className={classes}>
      <Label htmlFor={id} error={error}>
        {label}
      </Label>
      <input
        id={id}
        name={name}
        className="text-input"
        type={type}
        value={value}
        onChange={onChange}
        {...props}
      />
      <InputFeedback error={error} />
    </div>
  );
};

const contactForm = props => {
  const { values, touched, errors, handleChange, handleBlur } = props;
  return (
    <section className={styles.sectionContact} id="contact">
      <div className={styles.titleContact}>
        <h2>Contact</h2>
        <a href="mailto:kasina.adrian@gmail.com">kasina.adrian(at)gmail.com</a>
      </div>
      <form
        // onSubmit={handleSubmit}
        className={styles.contactForm}
        action="https://usebasin.com/f/247e87674541"
        method="POST"
      >
        <TextInput
          id="name"
          name="name"
          type="text"
          label="Name"
          placeholder="Your name"
          error={touched.name && errors.name}
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <TextInput
          id="email"
          name="email"
          type="email"
          label="Email"
          placeholder="Enter your email"
          error={touched.email && errors.email}
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <TextInput
          id="subject"
          name="subject"
          type="text"
          label="Subject"
          placeholder="Subject"
          error={touched.subject && errors.subject}
          value={values.subject}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <label htmlFor="message">Message</label>
        <Field
          component="textarea"
          name="message"
          rows="10"
          className={styles.textarea}
          id="message"
          type="text"
          placeholder="Leave a message"
          error={touched.message && errors.message}
          value={values.message}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {errors.message && touched.message ? (
          <div className={styles.error}>{errors.message}</div>
        ) : null}
        <button type="submit">Submit</button>
      </form>
    </section>
  );
};

export default formikEnhancer(contactForm);
