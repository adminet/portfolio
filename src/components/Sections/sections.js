import * as React from "react";
import Header from "../Header/header.js";
import About from "../About/about.js";
import Skills from "../Skills/skills.js";
import Portfolio from "../Portfolio/portfolio.js";
import Contact from "../Contact/contact.js";
import styles from "./sections.scss";

class Sections extends React.Component {
  render() {
    return (
      <div className={styles.container}>
        <Header />
        <About />
        <Skills />
        <Portfolio />
        <Contact />
      </div>
    );
  }
}

export default Sections;
