import * as React from "react";
import ReactDOM from "react-dom";
import "normalize.css";
import "./index.scss";
import App from "./components/App/App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
